@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Teori</div>

                <div class="card-body">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            @foreach ($result as $key => $val)
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#{{ $val[1] }}" aria-expanded="true" aria-controls="{{ $val[1] }}">
                                    {{ $val[0] }}
                                    </button>
                                </h2>
                            </div>
                            <div id="{{ $val[1] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    {!! $val[2] !!}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
