@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Enam</div>
                <div class="card-body">
                    <iframe src="https://pastebin.com/embed_iframe/7GBAJ2jF?theme=dark" style="border:none;width:100%; height:300px;"></iframe>
                    <h2>Input</h2>
                    <code>
                        {{ $input }}
                    </code>
                    <hr>
                    <h2>Output Format Judul</h2>
                    <code>
                        {{ $output_judul }}
                    </code>
                    <hr>
                    <h2>Output Format Biasa</h2>
                    <code>
                        {{ $output_biasa }}
                    </code>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
