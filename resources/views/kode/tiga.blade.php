@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Tiga</div>
                <div class="card-body">
                    <iframe src="https://pastebin.com/embed_iframe/TCrnJehT?theme=dark" style="border:none;width:100%; height=350px;"></iframe>
                    <h2>Hasil element array kedua</h2>
                    <code>{!! $hasil[2] !!}</code>
                    <hr>
                    <h2>Hasil element array lima</h2>
                    <code>{!! $hasil[5] !!}</code>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
