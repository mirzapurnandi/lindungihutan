<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JawabanKodeController;
use App\Http\Controllers\JawabanSqlController;
use App\Http\Controllers\JawabanTeoriController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/teori', [JawabanTeoriController::class, 'index'])->name('teori.index');

Route::group(
    ['prefix' => 'kode'],
    function () {
        Route::get('/', [JawabanKodeController::class, 'index'])->name('kode.index');
        Route::get('/satu', [JawabanKodeController::class, 'satu'])->name('kode.satu');
        Route::get('/dua', [JawabanKodeController::class, 'dua'])->name('kode.dua');
        Route::get('/tiga', [JawabanKodeController::class, 'tiga'])->name('kode.tiga');
        Route::get('/empat/{jumlah?}', [JawabanKodeController::class, 'empat'])->name('kode.empat');
        Route::get('/lima', [JawabanKodeController::class, 'lima'])->name('kode.lima');
        Route::get('/enam', [JawabanKodeController::class, 'enam'])->name('kode.enam');
        Route::get('/tujuh', [JawabanKodeController::class, 'tujuh'])->name('kode.tujuh');
    }
);

Route::group(
    ['prefix' => 'sql'],
    function () {
        Route::get('/', [JawabanSqlController::class, 'index'])->name('sql.index');
        Route::get('/pilihan/{id}', [JawabanSqlController::class, 'pilihan'])->name('sql.pilihan');
    }
);
