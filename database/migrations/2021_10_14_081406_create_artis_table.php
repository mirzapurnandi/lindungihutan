<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artis', function (Blueprint $table) {
            $table->string('kd_artis', 100);
            $table->string('nm_artis', 100);
            $table->string('jk', 100);
            $table->bigInteger('bayaran')->unsigned();
            $table->integer('award')->unsigned();
            $table->string('negara', 10);
            $table->primary('kd_artis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artis');
    }
}
