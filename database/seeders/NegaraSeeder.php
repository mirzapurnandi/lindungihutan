<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NegaraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('negara')->insert(
            [
                [
                    'kd_negara' => 'AS',
                    'nm_negara' => 'AMERIKA SERIKAT'
                ],
                [
                    'kd_negara' => 'HK',
                    'nm_negara' => 'HONGKONG'
                ],
                [
                    'kd_negara' => 'ID',
                    'nm_negara' => 'INDONESIA'
                ],
                [
                    'kd_negara' => 'IN',
                    'nm_negara' => 'INDIA'
                ]
            ]
        );
    }
}
