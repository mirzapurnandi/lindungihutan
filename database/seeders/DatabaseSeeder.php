<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ArtisSeeder::class,
            FilmSeeder::class,
            GenreSeeder::class,
            NegaraSeeder::class,
            ProduserSeeder::class,
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
