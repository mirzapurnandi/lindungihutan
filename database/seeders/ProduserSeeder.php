<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produser')->insert(
            [
                [
                    'kd_produser' => 'P001',
                    'nm_produser' => 'MARVEL',
                    'international' => 'YA'
                ],
                [
                    'kd_produser' => 'P002',
                    'nm_produser' => 'HONGKONG CINEMA',
                    'international' => 'YA'
                ],
                [
                    'kd_produser' => 'P003',
                    'nm_produser' => 'RAPI FILM',
                    'international' => 'TIDAK'
                ],
                [
                    'kd_produser' => 'P004',
                    'nm_produser' => 'PARKIT',
                    'international' => 'TIDAK'
                ],
                [
                    'kd_produser' => 'P005',
                    'nm_produser' => 'PARAMOUNT PICTURE',
                    'international' => 'YA'
                ]
            ]
        );
    }
}
