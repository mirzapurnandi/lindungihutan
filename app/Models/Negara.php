<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    use HasFactory;

    protected $table = "negara";

    protected $fillable = [
        'kd_negara',
        'nm_negara'
    ];

    protected $primaryKey = 'kd_negara';

    protected $keyType = 'string';

    public function artis()
    {
        return $this->hasMany(Artis::class, 'negara');
    }
}
