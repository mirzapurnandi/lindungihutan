<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $table = "genre";

    protected $fillable = [
        'kd_genre',
        'nm_genre'
    ];

    protected $primaryKey = 'kd_genre';

    protected $keyType = 'string';
}
