<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artis extends Model
{
    use HasFactory;

    protected $table = "artis";

    protected $fillable = [
        'kd_artis',
        'nm_artis',
        'jk',
        'bayaran',
        'award',
        'negara'
    ];

    protected $primaryKey = 'kd_artis';

    protected $keyType = 'string';

    public function negara()
    {
        return $this->belongsTo(Negara::class, 'negara');
    }

    public function film()
    {
        return $this->hasMany(Film::class, 'artis');
    }
}
