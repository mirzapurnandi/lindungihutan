<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Artis;
use App\Models\Negara;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JawabanSqlController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        $data = [
            [1, 'Tampilkan nama film dan nominasi dari nominasi yang terbesar', 1],
            [2, 'Tampilkan nama film dan nominasi yang paling banyak mendapatkan nominasi', 1],
            [3, 'Tampilkan nama film dan nominasi yang tidak dapat nominasi', 1],
            [4, 'Tampilkan nama film dan pendapatan urut dari yang terkecil', 1],
            [5, 'Tampilkan nama film dan pendapatan yang terbesar', 1],
            [6, 'Tampilkan rata2 pendapatan film keseluruhan', 1],
            [7, 'Tampilkan nama film dan artis yang memiliki award terbanyak', 0],
            [8, 'Tampilkan rata2 nominasi film keseluruhan', 1],
            [9, 'Tampilkan nama film yang huruf depannya ‘i’', 1],
            [10, 'Tampilkan nama film yang huruf terakhir ‘n’', 1],
            [11, 'Tampilkan nama film yang mengandung huruf ‘c’', 1],
            [12, 'Tampilkan nama film dengan pendapatan terbesar mengandung huruf ‘s’', 1],
            [13, 'Tampilkan nama film yang artisnya berasal dari negara hongkong', 1],
            [14, 'Tampilkan nama film yang artisnya bukan berasal dari negara yang tidak mengandung huruf ‘o’', 1],
            [15, 'Tampilkan negara mana yang paling banyak filmnya', 1],
            [16, 'Tampilkan data negara dengan jumlah filmnya', 0],
            [17, 'Tampilkan nama film dengan artis bayaran terendah', 1],
            [18, 'Tampilkan nama artis yang tidak pernah bermain film', 1],
            [19, 'Tampilkan nama artis yang paling banyak bermain film', 1],
            [20, 'Tampilkan nama artis yang bermain film dengan genre drama', 1],
            [21, 'Tampilkan nama artis yang bermain film dengan genre horror', 1],
            [22, 'Tampilkan produser yang tidak punya film', 0],
            [23, 'Tampilkan produser film yang memiliki artis termahal', 0],
            [24, 'Tampilkan produser yang memiliki banyak artis', 0],
            [25, 'Tampilkan nama film yang dibintangi oleh artis yang huruf depannya ‘j’', 0],
            [26, 'Tampilkan nama produser yang berskala internasional', 0],
            [27, 'Tampilkan berapa data produser berskala internasional', 0],
            [28, 'Tampilkan jumlah film dari masing2 produser', 0],
            [29, 'Tampilkan jumlah nominasi dari masing2 produser', 0],
            [30, 'Tampilkan jumlah pendapatan produser marvel secara keseluruhan', 0],
            [31, 'Tampilkan jumlah pendapatan produser yang skalanya tidak international', 0]
        ];

        return view('sql.index', [
            'result' => $data
        ]);
    }

    public function pilihan(Request $request)
    {
        $pilihan = $request->id;

        return $this->{'jawaban' . $pilihan}();
    }

    public function jawaban1()
    {
        $result = Film::orderByDesc('nominasi')->orderBy('nm_film')->get();
        return $result;
    }

    public function jawaban2()
    {
        $result = Film::orderByDesc('nominasi')->get();
        return $result;
    }

    public function jawaban3()
    {
        $result = Film::where('nominasi', 0)->get();
        return $result;
    }

    public function jawaban4()
    {
        $result = Film::orderBy('pendapatan')->get();
        return $result;
    }

    public function jawaban5()
    {
        $result = Film::orderByDesc('pendapatan')->get();
        return $result;
    }

    public function jawaban6()
    {
        $result = Film::avg('pendapatan');
        return $result;
    }

    public function jawaban7()
    {
        return 'Belum ada Jawaban';
    }

    public function jawaban8()
    {
        $result = Film::avg('nominasi');
        return $result;
    }

    public function jawaban9()
    {
        $result = Film::where('nm_film', 'like', 'i%')->get();
        return $result;
    }

    public function jawaban10()
    {
        $result = Film::where('nm_film', 'like', '%n')->get();
        return $result;
    }

    public function jawaban11()
    {
        $result = Film::where('nm_film', 'like', '%c%')->get();
        return $result;
    }

    public function jawaban12()
    {
        $result = Film::orderByDesc('pendapatan')->where('nm_film', 'like', '%s%')->first();
        return $result;
    }

    public function jawaban13()
    {
        $result = Film::select('film.*', 'artis.negara', 'artis.nm_artis')
            ->leftJoin('artis', 'artis.kd_artis', '=', 'film.artis')
            ->where('artis.negara', 'HK')
            ->get();
        return $result;
    }

    public function jawaban14()
    {
        $result = Film::select('film.*', 'artis.negara', 'artis.nm_artis')
            ->leftJoin('artis', 'artis.kd_artis', '=', 'film.artis')
            ->leftJoin('negara', 'negara.kd_negara', '=', 'artis.negara')
            ->where('negara.nm_negara', 'not like', '%o%')
            ->get();
        return $result;
    }

    public function jawaban15()
    {
        $result = Film::select(DB::raw('count(artis.negara) as total, artis.negara'))
            ->leftJoin('artis', 'artis.kd_artis', '=', 'film.artis')
            ->groupBy('artis.negara')
            ->get();
        return $result;
    }

    public function jawaban16()
    {
        $result = Negara::with('artis')->get();
        return $result;
    }

    public function jawaban17()
    {
        $result = Film::select('artis.nm_artis', 'film.nm_film', 'artis.bayaran')
            ->leftJoin('artis', 'artis.kd_artis', '=', 'film.artis')
            ->orderBy('artis.bayaran')
            ->first();
        return $result;
    }

    public function jawaban18()
    {
        $result = Artis::withCount('film')->get();
        return $result;
    }

    public function jawaban19()
    {
        $result = Artis::withCount('film')->get();
        return $result;
    }

    public function jawaban20()
    {
        $result = Film::with('artisss')->where('genre', 'G004')->get();
        return $result;
    }

    public function jawaban21()
    {
        $result = Film::with('artisss')->where('genre', 'G002')->get();
        return $result;
    }

    public function jawaban22()
    {
        return 'jawaban 1';
    }

    public function jawaban23()
    {
        return 'jawaban 1';
    }

    public function jawaban24()
    {
        return 'jawaban 1';
    }

    public function jawaban25()
    {
        return 'jawaban 1';
    }

    public function jawaban26()
    {
        return 'jawaban 1';
    }

    public function jawaban27()
    {
        return 'jawaban 1';
    }

    public function jawaban28()
    {
        return 'jawaban 1';
    }

    public function jawaban29()
    {
        return 'jawaban 1';
    }

    public function jawaban30()
    {
        return 'jawaban 1';
    }

    public function jawaban31()
    {
        return 'jawaban 1';
    }
}
