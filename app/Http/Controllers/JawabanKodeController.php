<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawabanKodeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        $data = [
            ['Tuliskan tag untuk mengirim file dan memasukkan data kedalam basis data!', 'satu', 0],
            ['Tuliskan code untuk melakukan increment sebuah variabel dalam looping?', 'dua', 1],
            ['Tuliskan code untuk menampilkan sebuah element array?', 'tiga', 1],
            ['Buat deret bilangan fibonacci (angka 1-100)?', 'empat', 1],
            ['Tulis code untuk menampilkan deret angka kelipatan 4 (angka 1-100)?', 'lima', 1],
            ['Tulis code untuk, Input : “SeLamAT PAGi semua halOo”', 'enam', 1],
            ['Tulis code untuk, Input : “aaabbcccaaaac”', 'tujuh', 0]
        ];

        return view('kode.index', [
            'result' => $data
        ]);
    }

    public function satu()
    {
        return "Kosong";
    }
    public function dua()
    {
        //Perulangan menggunakan for
        $hasil_satu = '';
        for ($i = 0; $i <= 10; $i++) {
            $hasil_satu .= $i . '<br>';
        }

        //perulangan menggunakan foreach
        $data_buah = ["mangga", "Durian", "Jeruk", "Apel", "Salak", "Pepaya", "Manggis", "Anggur"];
        $increment = 0;
        $hasil_dua = '';
        foreach ($data_buah as $key => $val) {
            $increment++;

            $hasil_dua .= $increment . '<br>';
        }
        return view('kode.dua', [
            'hasil_satu' => $hasil_satu,
            'hasil_dua' => $hasil_dua
        ]);
    }
    public function tiga()
    {
        // isi element dimulai dari 0 sampai n
        $data_buah = ["mangga", "Durian", "Jeruk", "Apel", "Salak", "Pepaya", "Manggis", "Anggur"];
        /*
            hasil dari $data_buah dapat di lihat seperti ini
            [0 => "mangga", 1 => "Durian", 2 => "Jeruk", 3 => "Apel", 4 => "Salak", 5 => "Pepaya", 6 => "Manggis", 7 => "Anggur"]
        */

        //memunculkan element array kedua yaitu Jeruk
        //echo $data_buah[2];

        //memunculkan element array lima yaitu Pepaya
        //echo $data_buah[5];
        return view('kode.tiga', [
            'hasil' => $data_buah
        ]);
    }
    public function empat(Request $request)
    {
        $bilangan = $request->jumlah ? $request->jumlah : 5;
        $result = $this->fibonacci($bilangan);

        return view('kode.empat', [
            'hasil' => $result
        ]);
    }

    public function fibonacci($jumlah)
    {
        # array tampung
        $fibonacci = [];

        if ($jumlah < 0 || $jumlah > 100) {
            # hentikan fungsi jika $jumlahBilangan kurang dari 0 dan melebihi 100
            return $fibonacci;
        }

        for ($i = 0; $i < $jumlah; $i++) {
            if ($i < 2) {
                $fibonacci[$i] = $i;
            } else {
                $fibonacci[$i] = $fibonacci[$i - 1] + $fibonacci[$i - 2];
            }
        }

        return $fibonacci;
    }

    public function lima()
    {
        $hasil = '';
        for ($i = 0; $i <= 100; $i++) {
            if ($i % 4 == 0) {
                $hasil .= $i . ' ';
            }
        }

        return view('kode.lima', [
            'hasil' => $hasil
        ]);
    }
    public function enam()
    {
        $input = "SeLamAT PAGi semua halOo";

        //Ubah dulu nilai inputan ke huruf kecil semua
        $change = strtolower($input);

        //lalu ubah setiap kata diawali huruf Besar
        $output_judul = ucwords($change);

        //menampilkan inputan hanya huruf besar di Awal saja
        $output_biasa = ucfirst($change);

        return view('kode.enam', [
            'input' => $input,
            'output_judul' => $output_judul,
            'output_biasa' => $output_biasa,
        ]);
    }
    public function tujuh()
    {
        return 'kosong';
    }
}
